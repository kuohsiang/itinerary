//
//  TripModel.swift
//  itinerary
//
//  Created by alex on 2019/5/13.
//  Copyright © 2019 alex. All rights reserved.
//

import Foundation

class TripModel {
    let id: UUID
    var title: String
    
    init(title: String) {
        id = UUID()
        self.title = title
    }
}
