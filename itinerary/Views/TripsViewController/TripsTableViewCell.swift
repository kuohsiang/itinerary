//
//  TripsTableViewCell.swift
//  itinerary
//
//  Created by alex on 2019/5/16.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit

class TripsTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cardView.layer.shadowOpacity = 1
        cardView.layer.shadowOffset = CGSize.zero
        cardView.layer.shadowColor = UIColor.darkGray.cgColor
        cardView.layer.cornerRadius = 10
        
    }
    
    func setUp(tripModel: TripModel) {
        titleLabel.text = tripModel.title
    }

}
